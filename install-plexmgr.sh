#!/bin/bash

# update, upgrade, clean and autoremove
echo 'Running update, upgrade, clean and auto-remove...'
apt-get update && apt-get upgrade -y && apt-get clean && apt-get autoremove

# install python pip & flask
sudo apt-get install python-pip -y
sudo pip install flask
sudo pip install flash shelljob

# install git & usbmount & curl
echo 'Installing git, usbmount and curl...'
apt-get install git usbmount curl -y

# curl down Solo & give execute priv
echo 'Downloading and installing SOLO...'
curl -q https://raw.githubusercontent.com/timkay/solo/master/solo -o solo
sudo chmod a+x solo
sudo mv solo /usr/bin/solo

# pull down plexmgr git repo
echo 'Cloning the plexmgr repo...'
cd ~/
git clone https://dev247@bitbucket.org/dev247/plex-mgr.git plexmgr

# pull down Font-Awesome repo
echo 'Cloning the FontAwesome repo...'
cd ~/plexmgr/static
git clone https://github.com/FortAwesome/Font-Awesome.git Font-Awesome

# add current user to visudo for shutdown binary
echo 'Adding passwordless access to shutdown for user plexsrv...'
sudo echo 'plexsrv    ALL=NOPASSWD: /sbin/shutdown' >> /etc/sudoers
sudo echo 'plexsrv    ALL=NOPASSWD: /usr/bin/python /home/plexsrv/plexmgr/web.py' >> /etc/sudoers
sudo echo 'plexsrv    ALL=NOPASSWD: /sbin/kill' >> /etc/sudoers
sudo echo 'plexsrv    ALL=NOPASSWD: /home/plexsrv/plexmgr/stop.sh'>> /etc/sudoers
sudo echo 'plexsrv    ALL=NOPASSWD: /home/plexsrv/plexmgr/service/unmount.sh'>> /etc/sudoers

print 'Dont forget to setup your crontab'
print 'crontab -e'
print '*/1 * * * * /usr/bin/solo -port=3001 /home/plexsrv/plexmgr/service/usbmgr.sh'
print '* 6 * * 6 /usr/bin/solo -port=3002 /home/plexsrv/plexmgr/service/update.sh'
print '@reboot /home/plexsrv/plexmgr/start.sh'

print 'You also need a crontab running root for the update'
print 'crontab -u root -e'
print '* 6 * * 7 /usr/bin/solo -port=3033 /home/plexsrv/plexmgr/service/update-server.sh'

# completed
echo 'Setup complete, see ~/plexmgr for further info.'