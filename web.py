from flask import Flask, render_template, redirect, url_for, flash, Response
import os
app = Flask(__name__, static_folder='static', static_url_path='')


# ROUTES
@app.errorhandler(500)
def page_not_found(e):
	return render_template('500.html'), 500


@app.route('/')
def index():
	return render_template('index.html')

@app.route('/mobile')
def index():
	return render_template('mobile.html')

@app.route('/plexweb')
def manage():
	return redirect('http://plexserver:32400/web/')


@app.route('/file-manager')
def manage_usb():
	# functions for moving files over to usb, viewing usb file copy logs and status
	#if os.path.exists("/home/plexsrv/plexmgr/static/filemanager.log"):
	if os.path.exists("./static/filemanager.log"):
		logs = []
		#fmlog = open("/home/plexsrv/plexmgr/static/filemanager.log")
		fmlog = open("./static/filemanager.log")
		for line in fmlog:
			logs.append(line)
		return render_template('index.html', logs=logs)
	else:
		return render_template('index.html', error="Log file missing, probably because no files have been added. \nAsk your admin for details.")

@app.route('/file-manager-mobile')
def manage_usb():
	# functions for moving files over to usb, viewing usb file copy logs and status
	#if os.path.exists("/home/plexsrv/plexmgr/static/filemanager.log"):
	if os.path.exists("./static/filemanager.log"):
		logs = []
		#fmlog = open("/home/plexsrv/plexmgr/static/filemanager.log")
		fmlog = open("./static/filemanager.log")
		for line in fmlog:
			logs.append(line)
		return render_template('mobile.html', logs=logs)
	else:
		return render_template('mobile.html', error="Log file missing, probably because no files have been added. \nAsk your admin for details.")

@app.route('/restart')
def restart():
	import subprocess
	cmd = subprocess.Popen(['sudo', 'shutdown', '-r', 'now'], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	out,error = cmd.communicate()


@app.route('/shutdown')
def shutdown():
	import subprocess
	cmd = subprocess.Popen(['sudo', 'shutdown', '-P', 'now'], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	out,error = cmd.communicate()


@app.route('/help')
def help():
	return redirect('https://support.plex.tv/hc/en-us')


# INIT MAIN
if __name__ == '__main__':
	app.run('0.0.0.0', 80, debug=False)
