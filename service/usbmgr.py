#!/usr/bin/env python

import glob, os, subprocess, shutil
import datetime
from os.path import basename

if os.path.exists('/media/usb/movies') or os.path.exists('/media/usb/television'):
	# Open the log file with 'w' so that it is only logging current or last operations
	with open('/home/plexsrv/plexmgr/static/filemanager.log', 'w')as fmlog:
		fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + ' - Found USB. Starting analysis.\n')
		# Run operation for the movies folders
		if os.path.exists('/media/usb/movies'):
			for movie in glob.glob('/media/usb/movies/*.mp4'):
				moviefile= basename(movie)
				if os.path.exists(os.path.join('/home/plexsrv/Videos/movies/', moviefile)):
					fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + ' - Exists:\t\t' + moviefile + '\n')
				else:
					shutil.copy(movie, os.path.join('/home/plexsrv/Videos/movies/', moviefile))
					fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + ' - Added:\t\t' + moviefile + '\n')
		else:
			fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + " - No movie folder found on USB.\n")
	
		# Run operation for the television folders
		if os.path.exists('/media/usb/television'):
			for episode in glob.glob('/media/usb/television/*.mp4'):
				epfile = basename(episode)
				if os.path.exists(os.path.join('/home/plexsrv/Videos/television/', epfile)):
					fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + ' - Exists:\t\t' + epfile + '\n')
				else:
					shutil.copy(episode, os.path.join('/home/plexsrv/Videos/television/', epfile))
					fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + ' - Added:\t\t' + epfile + '\n')
		else:
			fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + " - No television folder found on USB.\n")

	
	f = open('/home/plexsrv/plexmgr/static/filemanager.log', 'a+')
	# Need to unmount the usb drive now
	cmd = subprocess.Popen(['sudo', './home/plexsrv/plexmgr/service/unmount.sh'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out,error = cmd.communicate()
	fmlog.write(str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")) + ' - Finished scanning USB. It is safe to unplug the drive.\n')
	fmlog.close()
	
